import java.util.*;

public class Answer {

	public static void main(String[] param) {
		/**
		 * // TODO!!! Solutions to small problems // that do not need an
		 * independent method!
		 * 
		 * // conversion double -> String String s1 = String.valueOf(Math.PI);
		 * System.out.println("Esime �l "+s1); // conversion String -> int int
		 * n1 = 0; try { n1 = Integer.parseInt("2016"); } catch
		 * (NumberFormatException e) { System.out.println(
		 * "P��dsin kinni teisendus vea"); e.printStackTrace(); } catch
		 * (Exception e) { e.printStackTrace(); } // "hh:mm:ss" Calendar
		 * rightNow = Calendar.getInstance(); // v�tan �reahise aka int hour =
		 * rightNow.get(Calendar.HOUR_OF_DAY); int minute =
		 * rightNow.get(Calendar.MINUTE); int second =
		 * rightNow.get(Calendar.SECOND); System.out.println("Teine �l " + hour
		 * + ":"+ minute +":"+ second); // cos 45 deg System.out.println(
		 * "Kolmas �l " +Math.cos(45));
		 * 
		 * // table of square roots for (int i =0; i<=100; i+=5){
		 * System.out.println("Kuues �l: Arv millest ruutjuur v�etakse on: "+ i
		 * + " ja ruutjuur on " + Math.sqrt(i)); }
		 * 
		 * 
		 * String firstString = "ABcd12"; String result =
		 * reverseCase(firstString); System.out.println("Seitsmes �l " +"\"" +
		 * firstString + "\" -> \"" + result + "\"");
		 * 
		 * // reverse string
		 * 
		 * System.out.println("7. String reversed"); String abc = "1234ab";
		 * for(int i = abc.length() - 1; i >= 0; i--){
		 * System.out.print(abc.charAt(i)); } System.out.println();
		 * 
		 * String s = "How  many	 words   here"; int nw = countWords(s);
		 * System.out.println(s + "\t" + String.valueOf(nw));
		 * 
		 * // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
		 */
		final int LSIZE = 100;
		ArrayList<Integer> randList = new ArrayList<Integer>(LSIZE);
		Random generaator = new Random();
		for (int i = 0; i < LSIZE; i++) {
			randList.add(new Integer(generaator.nextInt(1000)));
		}

		// minimal element

		// HashMap tasks:
		// create
		// print all keys
		// remove a key
		// print all pairs

		System.out.println("Before reverse:  " + randList);
		reverseList(randList);
		System.out.println("After reverse: " + randList);

		System.out.println("Maximum: " + maximum(randList));
	}

	/**
	 * Finding the maximal element.
	 * 
	 * @param a
	 *            Collection of Comparable elements
	 * @return maximal element.
	 * @throws NoSuchElementException
	 *             if <code> a </code> is empty.
	 */
	static public <T extends Object & Comparable<? super T>> T maximum(Collection<? extends T> a)
			throws NoSuchElementException {
		return Collections.max(a); // TODO!!! Your code here
	}

	/**
	 * Counting the number of words. Any number of any kind of whitespace
	 * symbols between words is allowed.
	 * 
	 * @param text
	 *            text
	 * @return number of words in the text
	 */
	public static int countWords(String text) {

		return new StringTokenizer(text).countTokens(); // TODO!!! new asja
														// selgeks tegema, annan
														// kohe kus asi �ra
														// teha??
	}

	/**
	 * Case-reverse. Upper -> lower AND lower -> upper.
	 * 
	 * @param s
	 *            string
	 * @return processed string
	 */
	public static String reverseCase(String s) {
		StringBuilder sb = new StringBuilder(); // lisa m�lusi ei vaja, k�ik
												// t��tab buffris
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (Character.isLowerCase(ch)) {
				sb.append(Character.toUpperCase(ch));
			} else {
				if (Character.isUpperCase(ch)) {
					sb.append(Character.toLowerCase(ch));
				} else {
					sb.append(ch);
				}
			}
		}
		return sb.toString(); // TODO!!! Your code here
	}

	/**
	 * List reverse. Do not create a new list.
	 * 
	 * @param list
	 *            list to reverse
	 */
	public static <T extends Object> void reverseList(List<T> list) throws UnsupportedOperationException {
		Collections.reverse(list);
	}
}
